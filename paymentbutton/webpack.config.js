const webpack = require("webpack");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
var HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");
const TerserPlugin = require("terser-webpack-plugin");
var fs = require("fs");

const config = {
  entry: {
    // index: "./src/index.js",
    injector: "./src/injector.js",
    embed: "./src/embed.js"
  },
  // publicPath: "/",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    library: "CellPay",
    libraryExport: "default",
    libraryTarget: "umd"
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9002
    // proxy: 'http://localhost:9002/'
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        terserOptions: {
          mangle: true,
          keep_fnames: false,
          keep_classnames: false
        }
      })
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      inlineSource: ".(js|css)$",
      // inject: true,
      inject: "head",
      chunks: ["injector"]
    }),
    new HtmlWebpackInlineSourcePlugin(),
    new CompressionPlugin({
      test: /\.js(\?.*)?$/i,
      // test: /\.(js|css|svg)$/,
      exclude: /\/node_modules/
    }),
    // new webpack.BannerPlugin({
    //   banner: fs.readFileSync("./LICENSE", "utf8"),
    //   entryOnly: true
    // })
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          // { loader: "", options: { injectType: "singletonStyleTag" } },
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.svg$/,
        use: "file-loader"
      },
      {
        test: /\.png$/,
        use: [
          {
            loader: "url-loader",
            options: {
              mimetype: "image/png"
            }
          }
        ]
      }
    ]
  }
};

module.exports = config;
