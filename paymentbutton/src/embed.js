import "./assets/styles/app.scss";

// https://gist.github.com/eddywashere/dc5627693cf9ff4355e0
(function(window, document) {
    "use strict";

    var embed = window.CellPayPayment;

    // var priceLoaded = false;
    var _ready = false;
    var iframe = null;
    var initCalled = false;

    function processQueue(args) {
        var params = [].slice.call(args),
            method = params.shift();

        if (embed[method]) {
            embed[method].apply(embed, params);
        } else {
            console.log("embed does not have a " + method + " function");
        }
    }

    embed.init = function(receiver_number, additional) {

        // if (initCalled){
        //     return;
        // }

        let { merchant_name, merchant_image_url } = additional;


        var cellpayParent = document.getElementById("cellpay-payment-button");

        var btn = document.createElement("button");
        btn.innerHTML = "Pay with CellPay";
        btn.setAttribute(
            "style",
            "background-color: #142E6B; color: white; outline: 0; border: 0; font-size: 16px; padding: 10px 35px; border-radius: 4px; cursor: pointer; position: relative;"
        );
        cellpayParent.appendChild(btn);

        var cellPayframeContainer = document.createElement("div");
        cellPayframeContainer.id = "cellpay-frame-container";
        cellPayframeContainer.className = "cellpay--window";

        iframe = document.createElement("iframe");
        iframe.setAttribute("frameBorder", 0);
        iframe.className = "payment-container";



        cellpayParent.appendChild(cellPayframeContainer);
        cellPayframeContainer.appendChild(iframe);


        var iframeDocument = iframe.contentWindow.document;

        iframeDocument.open();
        iframeDocument.write(embed.frameBody());
        iframeDocument.close();


        var head = iframeDocument.getElementsByTagName("head")[0];
        let script = iframeDocument.createElement("script");
        script.type = "text/javascript";
        script.charset = "utf-8";
        script.src = "//192.168.0.53/app.js";
        // script.async = 1;
        head.appendChild(script);



        var internalCallback = function(state){
            // Close Window
            if (state.success){
                // Status Passed
                additional.onSuccess(state);
                cellPayframeContainer.className = "cellpay--window";

                iframe.contentWindow.reset();
                // additional.onWindowClose();

            }else{
                // Status Failed
                additional.onError(state);
            }
        }



        // https://stackoverflow.com/a/23552375
        let _readyIncrement = 0;
        let _readyTimer = setInterval(() => {
            // Not using this techniuque anymore
            // https://stackoverflow.com/a/23552375

            // Checks if a variable has been loaded on to the iframe window
            // if it has been loaded; then script has been loaded
            // this variable is loaded from vuejs side
            if (iframe.contentWindow["booted"] === true) {
                clearInterval(_readyTimer);
                _ready = true;


                iframe.contentWindow.init({
                    merchant_name: merchant_name,
                    merchant_number: receiver_number,
                    merchant_image: merchant_image_url,
                })


                iframe.contentWindow.internalCallback = internalCallback
                iframe.contentWindow.live(additional.live)


            }

            if (++_readyIncrement > 20) {
                // Stop Trying to Start Intercom Frame After 20 Seconds
                clearInterval(_readyTimer);
            }
        }, 1000);




        btn.onclick = function() {
            // console.log("Button CLick");

            // if (priceLoaded){
                cellPayframeContainer.className = "cellpay--window cellpay--window__showing";
            // }else{
                // console.warn("Price data has not been initiallized. Use the `price` method to update the cost.");
            // }
        };

        cellPayframeContainer.onclick = function() {
            // cellPayframeContainer.setAttribute('style', 'visibility: hidden;');
            cellPayframeContainer.className = "cellpay--window";

            iframe.contentWindow.reset();
            additional.onWindowClose();

        };



        // Find and Inject Location
    };

    embed.frameBody = function() {
        return `<!DOCTYPE html><html lang=en><head><meta charset=utf-8><meta http-equiv=X-UA-Compatible content="IE=edge"><meta name=viewport content="width=device-width,initial-scale=1"><title>CellPay</title></head><body><div id="app"></div></body></html>`;
    }


    embed.price = function(data) {

        // https://stackoverflow.com/a/23552375
        let _readyIncrement = 0;

        let _readyTimer = setInterval(() => {
            // Not using this techniuque anymore
            // https://stackoverflow.com/a/23552375

            // Checks if a variable has been loaded on to the iframe window
            // if it has been loaded; then script has been loaded
            // this variable is loaded from vuejs side
            if (_ready) {
                iframe.contentWindow.cost_update(data);
                // priceLoaded = true;
                clearInterval(_readyTimer);
            }

            if (++_readyIncrement > 20) {
                // Stop Trying to Start Intercom Frame After 20 Seconds
                clearInterval(_readyTimer);
            }
        }, 1000);


    };

    for (var i in embed.q || []) {
        processQueue(embed.q[i]);
    }

    // swap original function with just loaded one
    window.embed = function() {
        processQueue(arguments);
    };
})(window, document);
