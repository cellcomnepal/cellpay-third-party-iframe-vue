var ManifestPlugin = require("webpack-manifest-plugin");
var HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");

module.exports = {
  lintOnSave: false,
  productionSourceMap: false,


  // publicPath: process.env.NODE_ENV === "production" ? "http://localhost:5000/" : "http://localhost:8080/",
  chainWebpack: config => {
    config.optimization.delete("splitChunks");
    config.plugins.delete("preload");
    config.plugins.delete("prefetch");

    // config
    //   .plugin('html')
    //   .tap(args => {
    //     args[0].inlineSource = '(\.css$)'
    //     return args
    //   })
  },
  configureWebpack: {
    plugins: [
      new ManifestPlugin({
        filter: file => {
          // Manifest File Only Generates .js and .css files so that it can be linked into while embedding
          return file.isAsset === false;
        }
      }),
      new HtmlWebpackInlineSourcePlugin()
    ],
    output: {
      filename: "[name].js",
      chunkFilename: "[name].js"
    }
  },
  css: {
    extract: false,
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/styles/_vars.scss"; @import "@/assets/styles/mixins/mixins.scss";`
      }
    }
  }
};
