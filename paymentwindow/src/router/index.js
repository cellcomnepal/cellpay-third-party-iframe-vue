import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";
// import About from '../views/About.vue';

import Login from '../views/Login.vue';
import Payment from '../views/Payment.vue';
import ConfirmPayment from '../views/ConfirmPayment.vue';

import store from '@/store/index.js'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Login
  },
  {
    path: "/payment",
    name: "payment",
    component: Payment,
    meta: { requiresAuth: true }
  },
  {
    path: "/confirmpayment",
    name: "confirmpayment",
    component: ConfirmPayment,
    meta: { requiresAuth: true }
  },

];

const router = new VueRouter({
  // mode: "history",
  // base: process.env.BASE_URL,
  routes
});


router.beforeEach((to, from, next) => {
  // console.log(this);

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state._sessionId) {
      next({
        path: '/',
        replace: true
        // query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})



export default router;



