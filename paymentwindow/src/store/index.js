import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        _merchant: {
            name: null,
            number: null,
            invoice_number: null,
            image: null
        },
        _sessionId: null,
        _price: 0,
        _accounts: [],
        _selectedAccount: 0,
        _profile: null,
        _currentPayment: null,
        _errors: []
    },

    mutations: {
        setSessionId(state, payload) {
            state._sessionId = payload;
        },
        setAccounts(state, payload) {
            state._accounts = payload;
        },
        setProfile(state, payload) {
            state._profile = payload;
        },
        setPrice(state, payload){
            state._price = payload.price;
            state._merchant.invoice_number = payload.invoice_id
        },
        setMerchant(state, payload){
            state._merchant = payload
        },
        setCurrentPayment(state, payload){
            state._currentPayment = payload;
        },
        setError(state, payload){
            state._errors.push(payload);
            // clear error
        },
        setSelectedAccount(state, payload){
            state._selectedAccount = payload
        },
        reset(state){
            state._merchant = {
                name: null,
                number: null,
                invoice_number: null,
                image: null
            }

            state._sessionId= null
            state._price= 0
            state._accounts= []
            state._selectedAccount= 0
            state._profile = {
                name: "",
                images: [],
                mobileNo: ""
            },
            state._currentPayment= null
            state._errors= []
        },
    },
    actions: {},
    modules: {}
});
