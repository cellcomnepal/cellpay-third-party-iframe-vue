import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";
import Notifications from "vue-notification";

Vue.config.productionTip = false;
Vue.use(Notifications);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

// Callled From Iframe;
window["booted"] = true;

window["live"] = null;

let instance = null;

window["live"] = function(state) {
  if (state === true) {
    instance = axios.create({
      baseURL: "https://web.cellpay.com.np/rest/",
      timeout: 30000
    });
  } else {
    instance = axios.create({
      baseURL: "https://test.cellpay.com.np/rest/",
      timeout: 30000
    });
  }
  Vue.use(VueAxios, instance);
};

window["init"] = function(obj) {
  let { merchant_name, merchant_number, merchant_image } = obj;

  store.commit("setMerchant", {
    name: merchant_name,
    number: merchant_number,
    image: merchant_image
  });
};

window["cost_update"] = function(price) {
  store.commit("setPrice", price);
};

// Create Func from Calling Window
window.internalCallback = null;


window.reset = function(){
  store.commit("reset");
}


// For Testing Purpose; So that Data Gets Filled When Loading Outside of Iframe
store.commit("setMerchant", {
  name: "Merchant Name",
  number: 9801977888,
  invoice_number: 123123213
});

store.commit("setPrice", {
  price: 4000,
  invoice_id: "_invoice_id"
});

window.live(false);
